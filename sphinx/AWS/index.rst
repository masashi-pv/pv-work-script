.. AWS K documentation master file, created by
   sphinx-quickstart on Thu Oct 12 22:39:47 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AWS K's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   samples
   compute/index
   storage/index
   database/index
   network/index
   management/index
   security/index
   application/index
   messaging/index





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
