<?php

// ======================================================================
//
// Simple functions for connecting and close mysql db.
// ======================================================================

require_once('DBConfig.php');

// Connect to db
function connect() {

    $link = mysql_connect(DB_SERVER,DB_USER,DB_PASSWD);
    if(!$link) {
        die("Can not connect ".DB_SERVER." : ".mysql_error());
    }

    if(!mysql_select_db(DB_NAME)) {
        die("Can not use ".DB_NAME." : ".mysql_error());
    }

    return $link;
}

// Close
function close($link) {
    mysql_close($link);
}

// Prepare
function prepare ($value) {

    return mysql_real_escape_string($value);
}
