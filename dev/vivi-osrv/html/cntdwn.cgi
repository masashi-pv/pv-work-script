#!/usr/local/bin/perl
###################################################
#cntdwn.cgi  カウントダウンプログラム
#                   2003.07.14  MARCO
#              edit 2003.07.15  MARCO
#
#  パーミッション
#              cntdwn.cgi(755)
#              $file(666)
###################################################

############　設定項目　##############
#ファイル名(パーミッションを666で設定)
$file='cntdwn.txt';
#ページ背景色
$page_bgcolor='#003311';
#文字色
$text_color1='#aabbbb';
#文字色(強調)
$text_color2='#ffaaaa';
#管理キー
$masterkey='password';
######################################

@dmax=(31,28,31,30,31,30,31,31,30,31,30,31);
#今日の日付を獲得
($sec,$min,$hour,$dd,$mm,$yy,$wd)=localtime();
$yy+=1900;
$mm++;
#1900年から今日までの日数を算出
$py=$yy;
$pm=$mm;
$pd=$dd;
culcdays();
$nowdays=$days;

#入力データを獲得
use CGI;
$query=new CGI;
$iy=$query->param('YY');
$im=$query->param('MM');
$id=$query->param('DD');
$ttl=$query->param('TITLE');
$sts=$query->param("status");
$ilno=$query->param("IDX");
$ikey=$query->param("key");

#書き込み
if($sts eq 'INS' && $iy !=0 && $im !=0 && $id !=0){
	open(NOTE,">>$file");
	print NOTE "$ilno\t$iy\t$im\t$id\t$ttl\t$ikey\n";
	close(NOTE);
}
#削除
if($sts eq 'DEL'){
	open(NOTE,"<$file");
	@DATA=<NOTE>;
	close(NOTE);
	$i=0;
	$match=0;
	foreach(@DATA){
		($lno,$yy,$mm,$dd,$title,$key)=split(/\t/,$_);
		chomp($key);
		if($ilno==$lno && ($ikey eq $key || $ikey eq $masterkey)){
			splice(@DATA,$i,1);
			$match=1;
		}
		$i++;
	}
	if($match==1){
		open(NOTE,">$file");
		print NOTE @DATA;
		close(NOTE);
	}
}

#ＨＴＭＬ作成
print "Content-type: text/html\n\n";
print "<HTML>\n<HEAD>\n";
print "<TITLE>Count Down</TITLE>\n";
print "</HEAD>\n<BODY bgcolor=$page_bgcolor text=$text_color1>\n";

#ファイルを読み込んで出力
open(NOTE,"<$file");
@DATA=<NOTE>;
close(NOTE);
$lineno=1;
foreach(@DATA){
	($lno,$yy,$mm,$dd,$title,$key)=split(/\t/,$_);
	#1900年からターゲット日までの日数を算出
	$py=$yy;
	$pm=$mm;
	$pd=$dd;
	culcdays();
	$targetdays=$days;
	$restdays=$targetdays-$nowdays;
	if($restdays>=0){
		print "<font size=6>$title</font><br>";
		print "<font size=2>($yy年$mm月$dd日)</font>";
		print "<font size=4>まで<br>あと </font>";
		print "<font size=8 color=$text_color2><b>$restdays</b></font>";
		print "<font size=4> 日</font><br>\n";
		print "<FORM ACTION=cntdwn.cgi METHOD=post>\n";
		print "<input type=hidden name=IDX value=$lno>\n";
		print "<input type=hidden name=status value=DEL>\n";
		print " Key:<input type=password name=key size=8>\n";
		print "<input type=submit value=削除><hr></FORM>\n";
	}
	if($lno>=$lineno){$lineno=$lno+1;}
}

#入力フォームの表示
print "<FORM ACTION=cntdwn.cgi METHOD=post>\n";
print "■入力<br>";
print "<font size=2>西暦<input type=text name=YY size=4 value=$iy>年\n";
print "<input type=text name=MM size=2 value=$im>月\n";
print "<input type=text name=DD size=2 value=$id>日<br>\n";
print "タイトル<input type=text name=TITLE size=20><br>\n";
print "管理キー<input type=password name=key size=8></font>\n";
print "<input type=hidden name=IDX value=$lineno>\n";
print "<input type=hidden name=status value=INS>\n";
print "<input type=submit value=送信></FORM>\n";

print "</BODY>\n</HTML>\n";
exit;

#1900年からの日数の計算
sub culcdays{
	$days=$pd;
	for($i=$pm-1;$i>0;$i--){
		if($i==2){
			if(isleep($py)==1){$days+=29;}
			else{$days+=28;}
		}
		else{
			$days+=$dmax[$i-1];
		}
	}
	for($i=$py-1;$i>1900;$i--){
			if(isleep($i)==1){$days+=366;}
			else{$days+=365;}
	}
}
#閏年の判定
sub isleep{
	my($y)=@_;
	if(($y%4==0 && $y%100!=0) || $y%400==0){
		return 1;
	}
	else{
		return 0;
	}
}

