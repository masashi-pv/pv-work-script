<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>My Wallet</title>
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/base.css">
</head>
<body>
<h2>My Wallet Meisai</h2>
<?php
    $server = "localhost";
    $mydb = "ACCOUNT_MASTER";
    $usr = "accountmaster";
    $pass = "ms09accountmaster";

    $link = mysql_connect($server, $usr, $pass);
    if(!$link) {
        die("cannot connect:" . mysql_error());
    }

    $db = mysql_select_db($mydb, $link);
    if(!$db) {
        die("cannot use $mydb" . mysql_error());
    }

    // create SQL Query
    $query = "select * from wallet";
    $result = mysql_query($query);
    if(!$result) {
       die("empty: " . mysql_error());
    }

?>

<table border-color="#FFFFFF">
<caption align=center>【my wallet】</caption>
<div>
<tr>
  <th size=100>No</th>
  <th>支払日</th>
  <th>名称</th>
  <th>カテゴリ</th>
  <th>サブカテゴリ</th>
  <th>支払方法</th>
  <th>支出</th>
  <th>収入</th>
  <th>残額</th>
  <th>備考</th>
</tr>
</div>
<div>
<?php
    $num = 1; 
    while($row = mysql_fetch_assoc($result)) {
        printf("<tr align=center>");
        printf("<td>%s</td>", $num);
        printf("<td>%s</td>", $row["registered_date"]);
        printf("<td align=left>%s</td>", $row["content"]);
        printf("<td align=left>%s</td>", $row["category"]);
        printf("<td align=left>%s</td>", $row["subcategory"]);
        printf("<td align=left>%s</td>", $row["method"]);
        printf("<td align=left>%s</td>", $row["out_values"]);
        printf("<td align=left>%s</td>", $row["in_values"]);
        printf("<td align=left>%s</td>", $row["total_values"]);
        printf("<td>%s</td>", $row["comment"]);
        printf("</tr>");
        $num++;
    }
?>
</div>
</table>
 
<?php
        mysql_free_result($result);
        mysql_close($link);
?>
<br>
<h2>-- <a href="./top.html">return</a> --<h2>

</body>
</html>
