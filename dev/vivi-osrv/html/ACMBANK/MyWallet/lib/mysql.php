<?php
//================================
// MySQL Class
//================================
class MySQL {
    var $m_Con;
    var $m_HostName = "";
    var $m_UserName = "";
    var $m_Password = "";
    var $m_Database = "";
    var $m_Rows = 0;

    function MySQL() {
        $filename = "/var/www/mysql.ini";
        if (!file_exists($filename)) {
            die("not exist ini file.");
        } else {
            $fp = fopen($filename, "r");
            if (!$fp) {
                die("can't open ini file.");
            } else {
                $this->m_HostName=trim(fgets($fp));
                $this->m_UserName=trim(fgets($fp));
                $this->m_Password=trim(fgets($fp));
                $this->m_Database=trim(fgets($fp));
            }
            fclose($fp);
        }
 
        $this->m_con = mysql_connect($this->m_HostName, $this->m_UserName, $this->m_Password);
        if (!$this->m_con){
            die("MySQL Connect Failure...");
        }
 
        if (!mysql_select_db($this->m_Database, $this->m_con)) {
            die("DB select Failure.. DB:{$this->m_Database}");
        }
    }

    function query($sql) {
        $this->m_Rows = mysql_query($sql, $this->m_con);
        if (!$this->m_Rows) {
            die("MySQL Error Occored. <br><b>{$sql}</b></br>" .mysql_errno().":".mysql_error());
        }
        return $this->m_Rows;
    }

    function fetch() {
        return mysql_fetch_array($this->m_Rows);
    }

     return mysql_affected_rows();
    }
  
    function cols() {
      return mysql_num_fields($this->m_Rows);
    }
  
    function rows() {
      return mysql_num_rows($this->m_Rows);
    }
  
    function free() {
      mysql_free_result($this->m_Rows);
    }
  
    function close() {
      mysql_close($this->m_Con);
    }
  
    function errors() {
      return mysql_errno().": ".mysql_error();
    }
  
    function errorno() {
      return mysql_errno();
    }

}
?>

