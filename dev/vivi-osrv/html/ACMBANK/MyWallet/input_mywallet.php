<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>My Wallet ||| \ |||</title>
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/base.css">
<script type="text/javascript">
function jump_back() {
    location.href="./top.html";
}
function numOnly() {
    st = String.fromCharCode(event.keyCode);
    if ("0123456789\b\r\t".indexOf(st, 0) < 0) return false;
    return ture;
}

function selectSub(select) {
    select.value

}

function check(select){
    if ( select.value == "1") {
        select.selectedIndex = 0;
        select.form.date.style.display = 'none';
    } else {
        select.selectedIndex = 1;
        select.form.date.style.display = 'block';
    }
//flg=(select.elements(category).selectedIndex==4);
//if (!flg) select.elements('n2').value='';
//select.elements('n2').parentNode.style.visibility=flg?'visible':'hidden';
}

</script>
</head>
<body>
<h2>Input My Wallet</h2>
<br>

<form name "form1" action="input_mywallet_action.php" method="post">
    日付 時間<br>
    <?php
        $now_date = date('Y-m-d');
        $now_time = date('H:i:s');
        printf("<input type=date name=input_date value=%s size=10 Autocomplete=off>", $now_date);
        printf(" <input type=time name=input_time step=1 value=%s size=10 Autocomplete=off><br>", $now_time);
    ?>
    カテゴリ<br>
    <select size="1" name="category" >
    <?php
        require_once("lib/mysql.php");
        $category = new MySQL;
        var_dump($category);
        $category->query("select category_name from mst_category");
        while($row = $category->fetch()) {
            printf("<OPTION value=%s size=50 ", $row["category_name"]);
            printf(">%s</OPTION>", $row["category_name"]);
        }
    ?>
    </select>
    <input type="text" name="n_category" size="10" Autocomplete=off />
    <input value="追加" type="button" onclick="add_category()" /><br>
    サブカテゴリ<br>
    <select size="1" name="subcategory" >
    <?php
        require_once("lib/mysql.php");
        $subcategory = new MySQL;
        var_dump($subcategory);
        $subcategory->query("select subcategory_name from mst_subcategory");
        while($row = $subcategory->fetch()) {
            printf("<OPTION value=%s size=10 ", $row["subcategory_name"]);
            printf(">%s</OPTION>", $row["subcategory_name"]);
        }
    ?>
    </select>
    <input type="text" name="n_subcategory" size="10" Autocomplete=off />
    <input value="追加" type="button" onclick="add_subcategory()" /><br>
    名称<br>
    <input type="text" name="content" size="10" Autocomplete=off /><br>
    収支<br>
    <select size="1" name="syusi">
        <OPTION value="out">支出</OPTION>
        <OPTION value="in">収入</OPTION>
    </select><br>
    支払方法<br>
    <input type="text" name="method" size="10" Autocomplete=off><br>
    金額<br>
    <input style="text-align: right; ime-mode: disabled;" type="number" name="value" value="0" size="10" maxlength="7" onkeydown="return numOnly()"><br>
    <input value="入力" type="submit" />
    <input value="取消" type="button" onclick="jump_back()" />
</form>


</body>
</html>

