<?php
require_once('notelib.php');
$id = specialchars($_GET['id']);
$title = specialchars($_GET['title']);
$desc = specialchars($_GET['desc']);

$action = $_GET['action'];
if($action == "create"){
	insertNote("新規メモ","");
}else if($action == "delete"){
	deleteNote($id);
}else if($action == "update"){
	updateNote($id,$title,$desc);
}

header("Content-Type:application/json");
#Apacheなどで動作させる場合は以下の2行の処理が必要です。（理由はP117コラムを参照してください。）
header("Cache-Control:no-cache, no-store");
header("Pragma:no-cache");
print json_encode(readNotes());
?>