ListBox = Class.create();

ListBox.prototype = {
	initialize: function(id){
		this.id = id;
		this.items = [];
		this.selectedIndex = -1;
		this.background = "#bbf";
		this.selectedBackground = "#88c";
	},

	refresh:function(){
		$(this.id).innerHTML = this._toHtml();

		for(var i=0;i<this.items.length;i++){
			var id = this.id + '_item_'+i;
			$(id).onclick = this._select.bind(this,i);
		}
		
		if(this.selectedIndex >= this.items.length){
			this.selectedIndex = this.items.length-1;
		}

		this._highlight(this.selectedIndex);
	},

	selectedIndexChanged:function(index){
	},

	selectedIndexChanging:function(index){
	},

	_toHtml:function(){
		var ret = "<table border='0' style='cursor:default;width:100%;' >";
		for(var i=0;i<this.items.length;i++){
			var id = this.id + '_item_'+i;
			ret += "<tr><td id='"+id+"' style='padding-left:10px;background:"+this.background+";'>"+
			this.items[i]+"</td></tr>";
		}
		ret += "</table>";
		return ret;
	},

	_select: function(index){
		this.selectedIndexChanging(index);
		this._highlight(index);
		this.selectedIndex = index;
		this.selectedIndexChanged(index);
	},
	
	_highlight: function(index){
		if(this.selectedIndex != -1){
			$(this.id+'_item_'+this.selectedIndex).style.background = this.background;
		}
		if(index != -1){
			$(this.id+'_item_'+index).style.background = this.selectedBackground;
		}
	}
};