<?php

class MySQL{

  var $m_Con;
  var $m_HostName = "";
  var $m_UserName = "";
  var $m_Password = "";
  var $m_Database = "";
  var $m_Rows = 0;

  function MySQL(){

    $filename = "/var/www/mysql_smpl.ini";

    if (!file_exists($filename)) {
      die("mysql.iniが存在しません。");
    }  else {
      $fp = fopen($filename, "r");
      if (!$fp) {
        die("mysql.iniファイルが開けません。");
      } else {
        $this->m_HostName=trim(fgets($fp));
        $this->m_UserName=trim(fgets($fp));
        $this->m_Password=trim(fgets($fp));
        $this->m_Database=trim(fgets($fp));
      }

      fclose($fp);
    }

    $this->m_Con = mysql_connect($this->m_HostName, $this->m_UserName, $this->m_Password);
    if (!$this->m_Con) {
      die("MYSQLの接続に失敗しました。");
    }

    if (!mysql_select_db($this->m_Database, $this->m_Con)) {
      die("データベースの選択に失敗しました。DB:{$this->m_Database}");
    }

  }

  function query($sql) {
    $this->m_Rows = mysql_query($sql, $this->m_Con);
    if (!$this->m_Rows) {
      die("MySQL でエラーが発生しました。<br><b>sql:{$sql} </br></b>" .mysql_errno().": ".mysql_error());
    }

    return $this->m_Rows;
  }

  function fetch() {
    return mysql_fetch_array($this->m_Rows);
  }

  function affected_rows() {
    return mysql_affected_rows();
  }

  function cols() {
    return mysql_num_fields($this->m_Rows);
  }

  function rows() {
    return mysql_num_rows($this->m_Rows);
  }

  function free() {
    mysql_free_result($this->m_Rows);
  }

  function close() {
    mysql_close($this->m_Con);
  }

  function errors() {
    return mysql_errno().": ".mysql_error();
  }

  function errorno() {
    return mysql_errno();
  }

}

?>


