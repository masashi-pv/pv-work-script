<?php
require_once("./mysql.php");
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<title>7-4 MySQLクラスでデータ表示</title>
</head>
<body>

<?php

$mysql = new MySQL;
$sql = "SELECT * FROM friends";
$mysql->query($sql);
while($row = $mysql->fetch()) {
  echo $row["no"];
  echo $row["name"];
  echo $row["birth"];
  echo $row["email"];
  echo "<br>";
}
?>
</body>
</html>

