=== EC2 ===
Name: {{ name }}
id:   {{ id }}
Private IP: {{ ip }}
instance type: {{ type }}
subnet: {{ subnet }}
Security Groups: {{ sg }}
Key: {{ key }}
AZ: {{ az }}
