import os
from jinja2 import Template, Environment, FileSystemLoader


template = Template('Hello {{ name }}')
template.render(name = 'sample User!')

print(template.render(name = 'sample!'))

tmpdir = './template'
tmpfile = 'template.txt'

tmpfile2 = 'ELB.rst' ## name, id, endpoint, sg
tmpfile3 = 'RDS.rst' ## name, endpoint, type, dbname, dbuser,sg
item1 = 'Bird'
item2 = 'Dog'
item3 = 'Cats'

# with open('sample.txt', 'w') as fout:
#     fout.write(tmpl.render(num=1 , test1=item1, test2=item2, test3=item3))
#     fout.write(tmpl.render(num=2 , test1=item1, test2=item2, test3=item3))
#     fout.write(tmpl.render(num=3 , test1=item1, test2=item2, test3=item3))


class EC2:
    """docstring for ."""
    def __init__(self, env):
        tmpfile1 = 'EC2.rst' ## name, id, ip, type, subnet, sg, key, az
        self.tmp_ec2 = env.get_template(tmpfile1)

    def get_resources(self):
        ## EC2
        self.ec2_name = 'prd01-tky-test-web'
        self.ec2_id = 'i-erihervhaeu98687'
        self.ec2_ip = '10.298.128.11'
        self.ec2_type = 't2.small'
        self.ec2_subnet = 'sub-o8wh9e8h9'
        self.ec2_sg = 'sg-sodiv9e8h'
        self.ec2_key = 'default'
        self.ec2_az = 'ap-northeast-1a'

    def convert_rst(self, fout):
        fout.write(self.tmp_ec2.render(name = self.ec2_name, id = self.ec2_id, ip = self.ec2_ip, type = self.ec2_type, subnet = self.ec2_subnet, sg = self.ec2_sg, key = self.ec2_key, az = self.ec2_az))

class ELB:
    """docstring for ."""
    def __init__(self, env):
        tmpfile2 = 'ELB.rst' ## name, id, ip, type, subnet, sg, key, az
        self.tmp_elb = env.get_template(tmpfile2)

    def get_resources(self):
        ## ELB
        self.elb_name = 'prd01-tky-test-web'
        self.elb_endpoint = 'amazon.soidfw.zoih98hosz.com'
        self.elb_sg = 'sg-so98qche8h'

    def convert_rst(self, fout):
        fout.write(self.tmp_elb.render(name = self.elb_name, endpoint = self.elb_endpoint, sg = self.elb_sg))


class RDS:
    """docstring for ."""
    def __init__(self, env):
        tmpfile3 = 'RDS.rst' ## name, id, ip, type, subnet, sg, key, az
        self.tmp_rds = env.get_template(tmpfile3)

    def get_resources(self):
        ## RDS
        self.rds_name = 'prd01-tky-test-db'
        self.rds_endpoint = 'RDS.cec02j0oja9.amazonaws.com'
        self.rds_type = 't2.small'
        self.rds_dbname = 'Samples'
        self.rds_dbuser = 'admin'
        self.rds_sg = 'sg-nsvkdosnv0989whv'

    def convert_rst(self, fout):
        fout.write(self.tmp_rds.render(
            name = self.rds_name,
            endpoint = self.rds_endpoint,
            type = self.rds_type,
            dbname = self.rds_dbname,
            dbuser = self.rds_dbuser,
            sg = self.rds_sg))


def main():
    env = Environment(loader=FileSystemLoader(tmpdir,encoding='utf-8'), autoescape=False)

    with open('service.rst', 'w') as fout:

        ## EC2
        ec2 = EC2(env)
        ec2.get_resources()
        ec2.convert_rst(fout)

        ## ELB
        elb = ELB(env)
        elb.get_resources()
        elb.convert_rst(fout)

        ## RDS
        rds = RDS(env)
        rds.get_resources()
        rds.convert_rst(fout)

        fout.close()

if __name__ == '__main__':
    main()

