
機能一覧


・tfstateファイルからsphinx読み取り形式にcsv出力
・sphinx読み取り形式のcsvファイルを読み込み
・読み込んだリソース情報とテンプレートからRSTファイル出力



クラス設計


ジョブflow

1. terraform apply実行
2. terraform stateファイル出力

3. sphinx用ファイル生成 *

4. sphinx rstファイル生成 *
5. sphinx deploy *

*3 インターフェース
bash create_sphinx_format.sh $1{TargetProject} $2{}

*4 インターフェース
bash create_sphinx_rst.sh $1{TargetProject} $2{}

*5 インターフェース
bash deploy_sphinx_project.sh $1{TargetProject} $2{}
