# -*- MySQL:-8 -*-
# import mysql.connector
import MySQLdb


class MySQL():
    """docstring for ClassName"""

    def __init__(self):
        self.conn = MySQLdb.connect(
            # user='root',
            # passwd='masashi',
            # host='0.0.0.0',
            # db='photo_library'
            user='dbadmin',
            passwd='ms09dbadmin',
            host='192.168.33.20',
            db='photo_library'
        )
        self.cur = self.conn.cursor()

    def insert_pict(self, data):
        # (Name, InstanceId, ImageId, InstanceType, KeyName, zone, PrivateIpAddress, State))
        sql = """INSERT INTO
        photo_library.pictures (
            f_name,
            r_path,
            maker,
            model,
            org_date,
            exif_ver,
            size
        )
        VALUES (
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
            );"""
        for d in data:
            try:
                print(sql % d)
                self.cur.execute(sql % d)
                self.conn.commit()
            except:
                self.conn.rollback()
                raise

    def update_bucket_location(self, bucket, location):
        sql = """UPDATE aws.s3
        SET
            location_constraint = '%s'
        WHERE
            name = '%s';"""
        try:
            self.cur.execute(sql % (bucket, location))
            self.conn.commit()
        except:
            self.conn.rollback()
            raise

