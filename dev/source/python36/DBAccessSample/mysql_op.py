import MySQLdb

# class OperateDB:
def login():
    con = MySQLdb.connect(
        user='root',
        passwd='masashi',
        host='0.0.0.0',
        db='photo_library'
    )
    con
    return con

def select_all(cur, t_name):
    sql = "select * from " + t_name + ";"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def insert_pict(cur, con, f_name, r_path, maker, model):
    sql = "insert into pictures (f_name, r_path, maker, model) values(" + f_name + "," + r_path + "," + maker + "," + model + ");"
    cur.execute(sql)
    print(sql)
    con.commit()

con = login()
cur = con.cursor()
insert_pict(cur, con, "'sample.jpg'", "'./../sample.jpg'", "'Nikon'", "'D900'")
list = select_all(cur, "pictures")
print(list)
