import os
import sys
# import MySQLdb
from PIL import Image
from PIL.ExifTags import TAGS

from mysql import MySQL

target_dir = "/Users/masashi/Pictures/lookme/shinsugita"


# def insert_pict(cur, con, f_name, r_path, exif_values):
#     sql = "insert into pictures (f_name, r_path, maker, model, exif_ver, size) \
#            values(" + f_name + ',' + r_path + ',' + exif_values + "');"
#     cur.execute(sql)
#     # print(sql)
#     con.commit()

def find_all_files(directory):
    for root, dirs, files in os.walk(directory):
        yield root
        for file in files:
            yield os.path.join(root, file)

def get_exif_of_image(file_path, exif_data):
    im = Image.open(file_path)
    exif_data = {
        'maker':'',
        'model':'',
        'date':'',
        'exif_ver':''
        # 'size':''
    }
    # exif_data = {}
    try:
        exif = im._getexif()
    except AttributeError:
        return {}

    if exif is None:
        return None

    for tag_id, value in exif.items():
        tag = TAGS.get(tag_id)

        if tag == "Make":       # 画像入力機器のメーカー名
            exif_data["maker"] = value
        elif tag == "Model":    # 画像入力機器のモデル名
            exif_data["model"] = value
        elif tag == "DateTimeOriginal":     # 原画像データの生成日時
            if value == '0000:00:00 00:00:00':
                exif_data["date"] = '1000-10-10 00:00:00'
            else:
                exif_data["date"] = value.replace(':','-',2)
        elif tag == "ExifVersion":       # Exif version
            exif_data["exif_ver"] = value.decode('utf-8')
        # elif tag == "EXImageSize":       # Image Size
        #     exif_data["size"] = value

    # print(str(maker) + str(model) + str(date) + str(version))
    # pict_data.append((
    #     fname,
    #     path,
    #     maker,
    #     model,
    #     date,
    #     exif_ver.decode('utf-8'),
    #     size
    # ))
    # exif_values = "'" + maker + "','" + model + "','" + date + "','" + exif_ver.decode('utf-8') + "','" + size
    return exif_data


db = MySQL()
exif = {}
data = []

for file in find_all_files(target_dir):
    if '.jpg' in file:
        print(file)
        start = file.rfind("/") + 1
        fname = file[start:]
        path = file[:start]
        exif = get_exif_of_image(file, exif)
        if exif is not None:
            data.append((
                fname,
                path,
                exif["maker"],
                exif["model"],
                exif["date"],
                exif["exif_ver"],
                os.path.getsize(file)
            ))


db.insert_pict(data)


