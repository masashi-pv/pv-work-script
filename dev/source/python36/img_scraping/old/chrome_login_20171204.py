# 作詞掲示板にログインしてお気に入りの詞を取得する
import time
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

# ユーザー名とパスワードの指定 --- (※1)
USER = "gv4m-snk@asahi-net.or.jp"
PASS = "hamu612"
# url_mypage = "https://lookme-e.com/shinsugita_ekimae/photos"
url_mypage = "https://lookme-e.com/shinsugita/photos"
print("マイページ=", url_mypage)


def login(url, user, passwd):
    chromedriver = "./chromedriver"
    driver = webdriver.Chrome(chromedriver)
    driver.implicitly_wait(20)
    # driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    driver.get(url)
    email_el = driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[2]/form/div/div[1]/input')
    email_el.clear()
    email_el.send_keys(user)

    passwd_el = driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[2]/form/div/div[2]/input')
    passwd_el.clear()
    passwd_el.send_keys(passwd)
    driver.implicitly_wait(20)

    driver.find_element_by_css_selector('#loginForm > div > div.signIn > form > div > div.input-append > div.submit > button').click()
    time.sleep(20)
    return driver

driver = login(url_mypage, USER, PASS)

# driver.get("https://lookme-e.com/shinsugita_ekimae/photos")
page_source = driver.page_source
# page_source= driver.page_source.encode('utf-8')
# links = driver.find_element_by_css_selector("#newFeed > div.feedInner > div.itemList > ul")
# links = driver.find_element_by_css_selector('ul."cfix popup-gallery"')
# for a in links:
#     href = a.get_attribute('src')
# title = a.text
# print("-", title, ">", href)
#
# print(page_source)
# soup=BeautifulSoup(page_source,"lxml")


soup = BeautifulSoup(page_source, "html.parser")
# print(soup)
li_list = soup.findAll("li", class_="picture_item")

print(li_list)
# print(soup.select_one("#newFeed > div.feedInner > div.itemList > ul").li)
# ul_list = soup.select("#newFeed > div.feedInner > div.itemList > ul:nth-child")
# ul_list = soup.select("#newFeed > div.feedInner > div.itemList > ul")
# ul_list = soup.select("#newFeed > div.feedInner > div.itemList > ul")
#
# pop = soup.find_all(class_='cfix popup-gallery')

#pictures_20458375 > div.photo > div.print_guide

for li in li_list:
    pid = li.attrs['id']
    cond = " > div.photo > div.zoomBtn"
    prm = "#" + pid + cond
    element = driver.find_element_by_css_selector(prm)
    #Controlキーを押下しながら"Gmail"のリンクをクリック
    actions = ActionChains(driver)
    actions.key_down(Keys.COMMAND)
    actions.click(element)
    actions.perform()
    time.sleep(20)


# for li in li_list:
#     print(li.attrs['id'])
#     pid = li.attrs['id']
#     # print(soup.select_one("#newFeed > div.feedInner > div.itemList > ul").li)
#     cond = {"pid '> div.photo > div.print_guide'"}
#     driver.find_element_by_css_selector(cond).click()


#newFeed > div.feedInner > div.itemList > ul:nth-child(4)
#pictures_20457739 > div.photo > div.print_guide

#newFeed > div.feedInner > div.itemList > ul:nth-child(2)

# print(soup.find_all(''))
# img_list = soup.find("li", class_="picture_item")
# img_list = soup.find_all("ul", class_="event_name first")
# img_list = soup.select("#newFeed > div.feedInner > div.itemList > ul.cfix popup-gallery > li")
# img_list = soup.find_all('img')
# for img in img_list:
    # print(img['src'])
    # print("li =", img.string)
# print(img_list['src'])
#pictures_23192964 > div.photo > div.print_guide
# print(soup.find_all("li", class_="popup-gallery).string"))
# print(soup.select_one(".cfix popup-gallery").string)
# soup = BeautifulSoup(res.text, "html.parser")
# link = soup.select("#newFeed > .feedInner > .itemList > li[0]")
driver.close()
