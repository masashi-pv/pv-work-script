import time
import datetime
import os
import sys
import base64
import urllib.request

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


# ユーザー名とパスワードの指定 --- (※1)
USER = "gv4m-snk@asahi-net.or.jp"
PASS = "hamu612"
args = sys.argv
target = args[1]
target_date = args[2]
start_page = int(args[3])
end_page = int(args[4])

today = datetime.date.today().isoformat()

if target == 'rina':
    page_base_url = "https://lookme-e.com/shinsugita/photos?page="
    url_mypage = "https://lookme-e.com/shinsugita/photos"
    out_base_dir = "/Users/masashi/Pictures/lookme/shinsugita/" + target_date +"/"
    # out_base_dir = "/Users/masashi/Pictures/lookme/shinsugita/20180524/"
    # url_mypage = "https://lookme-e.com/shinsugita/photos/group_photos/28348"
elif target == 'yuika':
    page_base_url = "https://lookme-e.com/shinsugita_ekimae/photos?page="
    url_mypage = "https://lookme-e.com/shinsugita_ekimae/photos"
    out_base_dir = "/Users/masashi/Pictures/lookme/shinsugita_ekimae/" + target_date +"/"
else:
    print('required target!!')
    sys.exit(1)

if os.path.isdir(out_base_dir):
    print('exist directory', out_base_dir)
else:
    os.mkdir(out_base_dir)

print("マイページ=", url_mypage)


# os.mkdirs(dname, exist_ok=True)

def login(url, user, passwd):
    chromedriver = "./chromedriver"
    driver = webdriver.Chrome(chromedriver)
    driver.implicitly_wait(20)

    driver.get(url)
    email_el = driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[2]/form/div/div[1]/input')
    email_el.clear()
    email_el.send_keys(user)

    passwd_el = driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[2]/form/div/div[2]/input')
    passwd_el.clear()
    passwd_el.send_keys(passwd)

    driver.find_element_by_css_selector('#loginForm > div > div.signIn > form > div > div.input-append > div.submit > button').click()
    time.sleep(5)
    return driver


def get_page_pictures(web_driver):
    page_source = web_driver.page_source
    soup = BeautifulSoup(page_source, "html.parser")

    list = soup.select_one("#newFeed > div.feedInner > div.itemList")
    main_window = web_driver.current_window_handle
    for pm in list:
        if pm.string is not None:
            if len(pm.string) > 1:
                dname = out_base_dir + pm.string
                if os.path.isdir(dname):
                    print()
                else:
                    os.mkdir(dname)
        else:
            lilist = pm.contents
            for li in lilist:
                if isinstance(li, type(pm)):
                    # print(dirname, "/",li.attrs['id'])
                    pid = li.attrs['id']
                    cond = " > div.photo > div.zoomBtn"
                    prm = "#" + pid + cond
                    element = web_driver.find_element_by_css_selector(prm)
                    actions = ActionChains(web_driver)
                    actions.key_down(Keys.COMMAND)
                    actions.click(element)
                    actions.perform()
                    web_driver.switch_to.window(web_driver.window_handles[1])
                    now_page = web_driver.page_source
                    now_soup = BeautifulSoup(now_page, "html.parser")
                    # time.sleep(3)
                    enc_link = now_soup.find('canvas', class_="decode").attrs['enc']
                    link = base64.b64decode(enc_link).decode('utf-8')
                    start = link.rfind("/")
                    end = link.find("?")
                    fname = link[start:end]
                    ffname = dname + fname
                    web_driver.get(link)
                    request = urllib.request.urlopen(link)
                    f = open(ffname, "wb")
                    f.write(request.read())
                    f.close()
                    web_driver.close()
                    # web_driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + Keys.NUM)
                    web_driver.switch_to.window(main_window)
                    # time.sleep(3)

driver = login(url_mypage, USER, PASS)

for u in range(start_page, end_page):
    page_url = page_base_url + str(u) + "#new"
    driver.get(page_url)
    get_page_pictures(driver)

# for li in li_list:
# li_list = soup.findAll("li", class_="picture_item")





driver.close()
