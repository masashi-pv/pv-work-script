import time
import os
import sys
import base64
import urllib.request
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

# phantom js
# PhantomJS本体のパス
user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36'
pjs_path = './phantomjs'
dcap = {
    "phantomjs.page.settings.userAgent" : user_agent,
    'marionette' : True
}
chromedriver = "./chromedriver"

# parameter settings
args = sys.argv
USER = args[1]
PASS = args[2]
start_page = int(args[3])
end_page = int(args[4])
# url_mypage = args[5]
# page_base_url = args[6]
# out_base_dir = args[7]

url_mypage = "https://lookme-e.com/shinsugita_ekimae/photos/28352"
out_base_dir = "/Users/masashi/Pictures/lookme/shinsugita_ekimae/20180423/"
page_base_url = "https://lookme-e.com/shinsugita_ekimae/photos/28352?page="


print("マイページ=", url_mypage)

# os.mkdirs(dname, exist_ok=True)

class Lookme:

    def __init__(self):
        # self.driver = webdriver.Chrome(chromedriver)
        self.driver = webdriver.PhantomJS(executable_path=pjs_path,desired_capabilities=dcap)
        self.driver.implicitly_wait(20)

    def login(self, url, user, passwd):
        """
        lookmeへのログイン
        :param url:
        :param user:
        :param passwd:
        :return:
        """
        self.driver.get(url)
        user_xpath = '//*[@id="loginForm"]/div/div[2]/form/div/div[1]/input'
        email_el = self.driver.find_element_by_xpath(user_xpath)
        email_el.clear()
        email_el.send_keys(user)

        passwd_xpath = '//*[@id="loginForm"]/div/div[2]/form/div/div[2]/input'
        passwd_el = self.driver.find_element_by_xpath(passwd_xpath)
        passwd_el.clear()
        passwd_el.send_keys(passwd)

        submit_selector = '#loginForm > div > div.signIn > form > div > div.input-append > div.submit > button'
        self.driver.find_element_by_css_selector(submit_selector).click()
        time.sleep(5)

        self.main_window = self.driver.current_window_handle

    def get_page_pictur_list(self, url):
        """
        ページ内の写真一覧を取得
        :param url:
        :return list:
        """
        self.driver.get(url)
        soup = BeautifulSoup(self.driver.page_source, "html.parser")
        list = soup.select_one("#newFeed > div.feedInner > div.itemList")
        # list = soup.findAll("li", class_="picture_item")
        return list

    # def get_pictures(self, list):
    #     """
    #     ページ内の写真取得
    #     :param list:
    #     :return:
    #     """
    #
    #     for pm in list:
    #         if pm.string is not None:
    #             if len(pm.string) > 1:
    #                 dname = pm.string
    #                 if os.path.isdir(dname):
    #                     print()
    #                 else:
    #                     os.mkdir(dname)
    #         else:
    #             lilist = pm.contents
    #
    #             for li in lilist:
    #                 if isinstance(li, type(pm)):
    #                     # print(dirname, "/",li.attrs['id'])

    def open_image(self, li):
        """
        対象写真ページへの接続
        :param li:
        :return:
        """

        # open original image
        pid = li.attrs['id']
        cond = " > div.photo > div.zoomBtn"
        prm = "#" + pid + cond
        element = self.driver.find_element_by_css_selector(prm)

        actions = ActionChains(self.driver)
        actions.key_down(Keys.COMMAND)
        actions.click(element)
        actions.perform()

    def download_image(self, dir):
        """
        対象画像のダウンロード
        :param djr:
        :return:
        """
        # move window
        self.driver.switch_to.window(self.driver.window_handles[1])
        page_soure = self.driver.page_source
        # get image url
        soup = BeautifulSoup(page_soure, "html.parser")
        # time.sleep(3)
        enc_link = soup.find('canvas', class_="decode").attrs['enc']
        link = base64.b64decode(enc_link).decode('utf-8')


        #
        start = link.rfind("/")
        end = link.find("?")
        fname = link[start:end]
        ffname = dir + fname
        self.driver.get(link)

        # image download
        request = urllib.request.urlopen(link)
        f = open(ffname, "wb")
        f.write(request.read())
        f.close()

        # close tab
        self.driver.close()
        # web_driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + Keys.NUM)
        self.driver.switch_to.window(self.main_window)
        # time.sleep(3)

    def __del__(self):
        self.driver.close()


def main():

    lookme = Lookme()
    lookme.login(url_mypage, USER, PASS)

    # 指定ページ数分繰り返し
    for n in range(start_page, end_page):
        page_url = page_base_url + str(n)
        list = lookme.get_page_pictur_list(page_url)

        # 画像数繰り返し
        for pm in list:
            if pm.string is not None:
                if len(pm.string) > 1:
                    dname = pm.string
                    if os.path.isdir(dname):
                        print()
                    else:
                        os.mkdir(dname)
            else:
                lilist = pm.contents
                for li in lilist:
                    if isinstance(li, type(pm)):
                        lookme.open_image(li)
                        lookme.download_image(dname)
                        lookme.open_image(li)

            # print(dirname, "/",li.attrs['id'])

        # lookme.get_page_pictures(list)

    # for li in li_list:
    # li_list = soup.findAll("li", class_="picture_item")

if __name__ == '__main__':
    main()

